### BUILD STEP
FROM rust:slim-bookworm as build

# Update container + install Rustup
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y build-essential curl make libssl-dev pkg-config zip libclang-dev wget tar graphviz

# Rustbook
RUN cargo install mdbook
RUN cargo install mdbook-mermaid
RUN cargo install mdbook-admonish
RUN cargo install mdbook-toc
RUN cargo install mdbook-katex
RUN cargo install mdbook-bib
RUN cargo install mdbook-pdf

### PACKAGE STEP
FROM debian:stable-slim
# copy over mdbook binaries
COPY --from=build /usr/local/cargo/bin/mdbook* /bin

# install openssl
RUN apt-get update && apt-get -y upgrade && apt-get -y install libssl-dev

# clear cache
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*